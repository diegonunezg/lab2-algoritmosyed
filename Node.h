#include "Token.h"

class Node{
    Token token;
    Node *next;

    public:
        Node(){
            next = NULL;
        }
        Node(Token token){
            this->token = token;
            this->next = NULL;
        }
        Node *get_next(){
            return next;
        }
        void set_next(Node *next){
            this->next = next;
        }
        void set_next(){
            next = NULL;
        }
        Token get_token(){
            return token;
        }
        void set_token(Token token){
            this->token = token;
        }
};
