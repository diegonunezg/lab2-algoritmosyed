#include "Node.h"

class Stack{
    Node *top;
    public:
        Stack(){
            top = NULL;
        }

        Node *get_top(){
            return top;
        }

        Node *pop(){
            Node *aux = top;
            top = top->get_next();
            return aux;
        }

        void push(Node *new_top){
            new_top->set_next(top);
            top = new_top;
        }

        bool isEmpty(){
            if (top == NULL) 
                return true;
            return false;
        }

        int count() {
            Node *current = top;
            int counter = 0;
            while (current != NULL) {
                counter++;
                current = current->get_next();
            }
            return counter;
        }

        void reverse() {
            Node *guard, *current, *x;
            guard = NULL;
            x = NULL;
            current = top;
            while (current != NULL) {
                current = current->get_next();
                x = pop();
                x->set_next(guard);
                guard = x;
            }
            top = guard;
        }

        void DisplayStack(){
            if (isEmpty()){
                cout << "No hay elementos en el stack para mostrar" << endl;
                return;
            }
            Node *current;
            current = top;
            string stack_text = "";

            while (current != NULL){
                stack_text += "|\t"+ current->get_token().getValue() +"\t|"+ "\n";
                current = current->get_next();
            }

            cout << "\nStack" << endl;
            cout << "_________________" << endl;
            cout << stack_text;
            cout << "-----------------" << endl;
        }

        void clean() {
            while (!isEmpty()) {
                pop();
            }
        }
};
